import Settings from 'settings';

const databaseConfig = new Settings(`${__dirname}/../config/config`);
const mongoModel = require(`${__dirname}/../service/mongodb`);

class Page {

  /**
   * ViewData
   * This function purpose for view data from database
   */
  viewData(req, res, next) {
    let models = new mongoModel('page');
    let opts = req.query || {};
    delete opts.accessToken;
  	models.find(opts, function(err, data){
  		res.status(200).json({ "message" : "Success View", "data" : data});
  	});
  }

  deleteData(req, res, next) {
    let models = new mongoModel('page');
    let opts = req.query || {};
    models.removeById(opts, function(err, data){
      res.status(200).json({ "message" : "Success Remove", "data" : data});
    });

  }

  updateData(req, res, next) {
    let models = new mongoModel('page');
    models.update (
                  { "_id" : "5ae498560288ae00946edce2" },
                  { $set : { urlImage: 'https://static.zara.net/photos/2018/V/0/1/p/4437/073/538/5/w/560/4437073538_1_1_1.jpg?ts=1524733457574' }
                  },
                  function( errdb, resdb ) {
                    //if ( err ) throw err;
                     console.log(errdb, resdb);

                  }
              );  

    res.status(200).json({ "message" : "Success Update", "data" : {}});
  }
}

module.exports = new Page();