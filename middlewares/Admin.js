import Settings from 'settings';
import bcrypt from 'bcrypt';

const constant = require(`${__dirname}/../config/constant`);
const databaseConfig = new Settings(`${__dirname}/../config/config`);
const mongoModel = require(`${__dirname}/../service/mongodb`);

class Admin {

  /**
   * ViewData
   * This function purpose for view data from database
   */
  homePage(req, res, next) {
      res.render('engine/index', {
        environment: process.env.NODE_ENV.toLowerCase() || 'development'
      });
  }

  /**
   * CheckSecret
   * This function purpose for checking Header there have clientKEY or not request this API
   */
  checkSecret(req, res, next) {
    if (!req.body.clientKey || !req.body.clientSecret) 
      return res.status(401).json({ code: 401, status: 'error', message: 'Unauthorized request.', data: null });

    if (!constant.secret[req.body.clientKey] || constant.secret[req.body.clientKey] != req.body.clientSecret) 
      return res.status(401).json({ code: 401, status: 'error', message: 'Unauthorized request.', data: null });

    if (constant.secret[req.body.clientKey] == req.body.clientSecret)
      next();
  }

  /**
   * Login
   * This function purpose for login to admin acccess level
   */
  login(req, res, next) {

    if (!req.body.username && !req.body.password)
      return res.status(401).json({ code: 401, status: 'error', message: 'Unauthorized request.', data: null });

    if (!constant.engine.account[req.body.username]) 
      return res.status(401).json({ code: 401, status: 'error', message: 'Can\'t find any user.', data: null });

    let isPasswordMatched = bcrypt.compareSync(req.body.password, constant.engine.account[req.body.username].password);
    console.dir(isPasswordMatched);
    if (!isPasswordMatched)
      return res.status(401).json({ code: 401, status: 'error', message: '', data: null });

    let dataUser = constant.engine.account[req.body.username];

    res.status(200).json({ code: 200, status: 'success', message: null, data: dataUser });
  }

}

module.exports = new Admin();