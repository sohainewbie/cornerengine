import config from './../config/config.js';

class Index {

  hello (req, res, next) {
    res.status(200).json(
      {
        "name": "Corner API Gateway",
        "version": "1.0.0"
      }
    );
  }

}

module.exports = new Index();