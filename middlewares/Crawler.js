import Settings from 'settings';
import request from 'request';
import cheerio from 'cheerio';
import fs from 'fs';
import sleep from 'sleep';


const services = new Settings(`${__dirname}/../config/services`);
const model = require(`${__dirname}/../models/page`);
const mongoModel = require(`${__dirname}/../service/mongodb`);

let opts = {};
let _ = require('lodash');

class Crawler {

  /**
   * CheckingApi
   * This function purpose for validate the token from diffbot is expired or not.
   */

  checkingApi(req, res, next) {
    let url = `${services.diffbot.apiUrl}/v3/crawl?token=${services.diffbot.key}`;
      request.get(url, (error, response, body) => {
      	let jb = JSON.parse(body);
      	if (error) return res.status(500).json(body); // if not response from the api
      	if ("errorCode" in jb) res.status(jb.errorCode).json({ "message" :jb.error}); // expired
      	next();
      });	
  }

  /**
   * fetchData
   * This function purpose for crawling the web and parse it.
   * ex: https://www.zara.com/id/en/woman-new-in-l1180.html?v1=805003
   */

  fetchData (req, res, next) {
    let urlSearch = req.query.url;
    let botSearch = req.query.bot;

    if(/^(http:\/\/www\.|https:\/\/www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(urlSearch)
        || ( botSearch == true || botSearch == 'true' )
      ){
      let models = new mongoModel('page');
      // if used diffbot
      if (botSearch == true || botSearch == 'true' ) {
        let typeSearch = 'product'; //article
        opts.url = `${services.diffbot.apiUrl}/v3/${typeSearch}?token=${services.diffbot.key}&url=`+encodeURIComponent(urlSearch);
        request.get(opts, (error, response, body) => {
            if (error) return res.status(500).json("something happend with the botdiff"); // if not response from the api
            let metadata = {}
            let jb = JSON.parse(body);
            let where = { url : urlSearch };
            if ("objects" in jb) {
                let obj = jb.objects[0];
                metadata = {
                    urlImage : obj.images[0]['url'],
                    title: obj.title,
                    desc : obj.text
                };
            }
            const saveModel = (where, data, cb) => {
                // UPDATEING DATA
                models.update( where , { $set : data }, function(errdb, resdb){
                    return cb(errdb, resdb);
                });
            };
            saveModel(where, metadata, function(errSave, resSave){
                if (errSave) return res.status(404).json({"message" : "something happend with the database"});
                return res.status(200).json({"message" : "Success"});
            })
         });
      }
      else {
        // manual crawling.
        request.get(urlSearch, (error, response, body) => {
          //  if (error) return res.status(500).json(body); // if not response from the api
            const parseHtml = (cb) => {
              //const result = [];
              let $ = cheerio.load(body);
              $('div.product-info-item-hashtag').each(function(i, e){
                if( i != 0) {
                  let urlDetail = $(this).prev().prev().find('._item').attr('href').split("?")[0];
                  let dataPrice = $(this).prev().find('._product-price').find('span').attr('data-price');
                  let price = dataPrice.replace(" IDR", "").replace(",", "");
                  let payload = {
                    urlDetail : urlDetail,
                    dataPrice: dataPrice,
                    price : price
                  };
                  var metadata = {
                          url: payload.urlDetail,
                          price: {
                              number : parseInt(payload.price),
                              string : payload.dataPrice
                          }
                        };
                  //let models = new model(metadata);
                  models.create(metadata, function(errdb, resdb){
                      console.log(errdb, resdb);
                      //sleep.sleep(5);
                  });
                  /*
                  // Manual get detail without diffbot
                  //const fetchDetail = (payload, cb) => {
                    request.get(payload.urlDetail, (errDetail, resDetail, bodyDetail) => {
                        $ = cheerio.load(bodyDetail);
                        let urlImg = $('a._seoImg').attr('href');
                        if(urlImg !== null && urlImg !== '') urlImg = `https:`+ urlImg;
                        let titleProduct = $(`h1.product-name`).text() || null;
                        let price = $(`div.price._product-price-name`) || null;
                        let description = $(`p.description`).text() || null;
                        let color = $(`span._colorName`).text() || null;
                        var metadata = {
                          title: titleProduct,
                          url: payload.urlDetail,
                          urlImage: urlImg,
                          color: color,
                          price: {
                              number : payload.price,
                              string : payload.dataPrice
                          },
                          desc: description
                        };
                        //result.push(metadata);
                        console.log("Simpan ke mongo");
                        console.log(metadata);
                        let models = new model(metadata);
                        models.save(function(err, res){
                            console.log(err, res);
                            sleep.sleep(5);
                        });
                    });*/
                }
              });
              return cb({});
            }
            parseHtml(function(resParse){
              console.log(resParse);
            });
            res.status(200).json({ "message" : "Success"});
        });
      }
    }else{
      res.status(400).json({ "message" : "Please input valid url"});  
    }
  }
}

module.exports = new Crawler();