/***
 * 
 * DEFAULT CONFIGURATION
 * FOR 
 ***/
const Config = {
  
  common: {
    port: 8080,
    mongodb: {
      corner: {
        hostAddress: 'mongodb://localhost:27017/corner'
      }
    }
  },

  development: {
    port: 8080,
    mongodb: {
      corner: {
        hostAddress: 'mongodb://localhost:27017/corner'
      }
    }
  },

  production: {
    port: 8080,
    mongodb: {
      corner: {
        hostAddress: 'mongodb://localhost:27017/corner'
      }
    }
  }

}

module.exports = Config;