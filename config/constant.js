/***
 * 
 * CONSTANT PARAMETER
 ***/
const Constant = {
  engine: {
        account: {
          superadmin:{
            name: 'Superadmin',
            password: '$2a$10$NFeWyys.eiAIPCis/.a2RuZkD45sP0OE.4O1mzQAVz0I0uyPDV7KK', // Eng1n3$$
            token: '6f004719be424817a68de028fa7fb4847845ae14'
          },
          sohai:{
            name: 'sohai',
            password: '$2a$10$QlY/0t1ScssBmRjFk8IwkunulcGloocnfsZmDR.xgdNo6BJq69cce', // s0h@iNewb13
            token: 'defaultuser'
          }          
        },
        token: {
          '6f004719be424817a68de028fa7fb4847845ae14': 'superadmin'
        }
  },
  bcrypt: { saltRounds: 10 },  
  secret: { engine: ':D' }
}

module.exports = Constant;