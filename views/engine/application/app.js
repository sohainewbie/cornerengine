'use strict';

/** ------------------------------
*
*		APPLICATION MODULES
*
------------------------------ **/

var modules = [
	'ngRoute',
	'ngResource',
	'angular-loading-bar',
	'toastr',
	'ajax',
	'auth',
	'ui.tinymce',
	'datatables',
	'datatables.bootstrap',

	'Login',
	'Dashboard',
	'Product'	
];

/** ------------------------------
*
*		INIT APPLICATION
*
------------------------------ **/

angular
	.module(config.NAME, modules)
	.config([ '$routeProvider', function ($routeProvider) {
			routes.forEach(function (r) {
				if (r && r.endpoint && r.routeParams)
					$routeProvider.when(r.endpoint, r.routeParams);
			});
			$routeProvider.otherwise({ redirectTo: '/' });
		}
	])
	.run(function ($rootScope, $location, auth) {
		$rootScope.$on('$routeChangeStart', function (event, next, current) {
			var dataCredentials = auth.getCredentials()		
			if (dataCredentials.type != 'engine')
				return $location.path('/login');
		});

		$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
			$rootScope.user = auth.getUser();
			$rootScope.redirectPage = function (path) {
				if (path.split('/')[0] == 'help') {
					$rootScope.pageSubActive = path.split('/')[1];
				}
				$location.path('/'+path);
			};
			$rootScope.loadContentWrapper = function () {
				$('#content-wrapper').css({ width: $(window).width() - 240 });
				$('.nav-tabs a').click(function (e) {
	  			e.preventDefault()
	  			$(this).tab('show');
				});
			};

			$rootScope.logout = function () {
				auth.logout();
				$location.path('/');
			};

			$rootScope.openChildMenu = function (id) {
				$('li#'+id).find('ul').slideDown();
				$('li#'+id).addClass("active");
			}

			$rootScope.openProfileMenu = function () {
				$('header ul li#profile ul').slideToggle();
			}
		});
	});
