'use strict';

/** ------------------------------
*
*		APPLICATION CONFIG
*
------------------------------ **/

var system = {
	common: {
		API_URL: 'http://localhost:8080'
	},

	development: {
		API_URL: 'http://localhost:8080'
	},

	production: {
		API_URL: 'http://localhost:8080'
	}	
};