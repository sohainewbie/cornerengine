'use strict';

angular
	.module('Login', [])
	.controller('Login', function ($rootScope, $scope, $location, ajax, auth, toastr) {
		$scope.model = {};
		$scope.pageActive = 'Login';

		var dataCredentials = auth.getCredentials()
		if (dataCredentials.type == 'engine')
			return $location.path('/');
		
		$scope.login = function () {
			$scope.model.clientKey = 'engine';
			$scope.model.clientSecret = ':D';

			if ($scope.model.username && $scope.model.password && $scope.model.username.length > 0 && $scope.model.password.length > 0) {
				ajax.post('/api/v1/engine/login', $scope.model).then(function (data) {
					if (data && data.token) {
						auth.setUser(data);
						$location.path('/');
					}
				});
			} else {
				toastr.error('Please enter your username and password.', 'Error');
			}

		}
	});