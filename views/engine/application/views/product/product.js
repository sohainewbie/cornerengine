'use strict';

angular
	.module('Product', [])
	.controller('Product', function ($rootScope, $scope, $location, ajax, auth, toastr, $routeParams) {
		$rootScope.loadContentWrapper();
		$scope.pageActive = 'Product';
		$scope.model = { product: {} };
		$scope.tinymceOptions = {
			plugins: 'link image code',
			toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
			height : '320'
		};


		$scope.loadData = function(){
			ajax.get('/v1/page/dataview?_id='+ $routeParams.id).then(function (data) { 
				$scope.model.product = data[0];
			});
		};

		$scope.loadProduct = function(){
			ajax.get('/v1/page/dataview').then(function (data) { $scope.product = data; });		
		};

	});