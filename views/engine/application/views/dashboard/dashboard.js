'use strict';

angular
	.module('Dashboard', [])
	.controller('Dashboard', function ($rootScope, $scope, ajax, auth) {
		var dataCredentials = auth.getCredentials()

		$rootScope.loadContentWrapper();		
		$scope.pageActive = 'home';
		$scope.checkuser = dataCredentials;
	});