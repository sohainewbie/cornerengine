'use strict';

angular
	.module('Logout', [])
	.controller('Logout', function ($rootScope, $scope, $location, ajax, auth, toastr) {
		auth.logout();
		$location.path('/');
	});