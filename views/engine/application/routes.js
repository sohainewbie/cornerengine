'use strict';

/** ------------------------------
*
*		APPLICATION ROUTES
*
------------------------------ **/
/*
var routes = [

	{ endpoint: '/', routeParams: { controller: 'Dashboard', templateUrl: '/engine/application/views/dashboard/dashboard.html' } },
	{ endpoint: '/campaign-report', routeParams: { controller: 'CampaignReport', templateUrl: '/engine/application/views/campaign_report/report.html' } },
	{ endpoint: '/stories', routeParams: { controller: 'Stories', templateUrl: '/engine/application/views/stories/stories.html' } },
	{ endpoint: '/stories/create', routeParams: { controller: 'Stories', templateUrl: '/engine/application/views/stories/create.html' } },
	{ endpoint: '/stories/edit/:id', routeParams: { controller: 'Stories', templateUrl: '/engine/application/views/stories/edit.html' } },	
	{ endpoint: '/login', routeParams: { controller: 'Login', templateUrl: '/engine/application/views/login/login.html' } },
	{ endpoint: '/career', routeParams: { controller: 'Career', templateUrl: '/engine/application/views/career/career.html' } },
	{ endpoint: '/investors', routeParams: { controller: 'Investors', templateUrl: '/engine/application/views/investors/investors.html' } },
	{ endpoint: '/transactions', routeParams: { controller: 'Transactions', templateUrl: '/engine/application/views/transactions/transactions.html' } },
	{ endpoint: '/lenders', routeParams: { controller: 'Lenders', templateUrl: '/engine/application/views/lenders/lenders.html' } },
	{ endpoint: '/download', routeParams: { controller: 'Download', templateUrl: '/engine/application/views/download/download.html' } },
	{ endpoint: '/survey', routeParams: { controller: 'Survey', templateUrl: '/engine/application/views/survey/survey.html' } }	
];*/

var routes = [

	{ endpoint: '/', routeParams: { controller: 'Dashboard', templateUrl: '/engine/application/views/dashboard/dashboard.html' } },
	{ endpoint: '/login', routeParams: { controller: 'Login', templateUrl: '/engine/application/views/login/login.html' } },
	{ endpoint: '/product', routeParams: { controller: 'Product', templateUrl: '/engine/application/views/product/view.html' } },
	{ endpoint: '/product/detail/:id', routeParams: { controller: 'Product', templateUrl: '/engine/application/views/product/detail.html' } }
];