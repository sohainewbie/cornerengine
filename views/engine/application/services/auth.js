'use strict';

/** ----------------------------
*
*	AUTH SERVICES
*
---------------------------- **/

angular
	.module('auth', [])
	.factory('auth', function (ajax, $http) {
		return {
			login: function (params, cb) {
				if (!params) params = {};

				params.clientId 	 = 'web';
				params.clientSecret  = ':)';
				params.grantType 	 = 'password';

				return AjaxService.post('/v1/auth/login', params);
			},

			setUser: function (user) {
				localStorage.user = JSON.stringify(user);
				localStorage.accessToken = user.token;
				localStorage.type = 'engine';
			},


			getUser: function () {
				if(!localStorage.user || !localStorage.type){
		      		localStorage.user = JSON.stringify({});
		        }
        		return JSON.parse(localStorage.user);
			},

			setCredentials: function (accessToken) {
				$http.defaults.headers.common['AccessToken'] = accessToken;
				$http.defaults.headers.common['Authorization'] = 'bearer ' + accessToken;
				localStorage.accessToken = accessToken;
			},

			getCredentials: function () {
				var dataCredentials = {'localstorege' : localStorage.accessToken , 'type' : localStorage.type};
				return dataCredentials;
			},

			clearCredentials: function () {
				localStorage.accessToken = null;
			},

			setRole: function (role) {
				localStorage.role = role;
			},

			getRole: function () {
				return localStorage.role;
			},

			clearRole: function () {
				localStorage.role = null;
			},

			logout: function () {
				localStorage.clear();
			}
		}
	});
