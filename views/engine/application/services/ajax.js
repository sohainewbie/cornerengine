'use strict';

/** ----------------------------
*
*	AJAX SERVICES
*
---------------------------- **/

angular
	.module('ajax', [])
	.factory('ajax', function ($http, toastr, $location) {
		/*const headerDict = {
		  'Content-Type': 'application/json',
		  'Accept': 'application/json',
		  'Access-Control-Allow-Headers': 'Content-Type',
		  'accessToken' : localStorage.accessToken
		}

		const requestOptions = {                                                                                                                                                                                 
		  headers: new Headers(headerDict), 
		};
		*/
		var AjaxService = {
			ajaxSuccess: function (args) {
				if (args && args.data && args.data.data)
					args.data = args.data.data;
				return args.data;
			},

			ajaxError: function (args) {
				if (args.status < 0) {
					toastr.error('Cannot connect to Server', 'Error');
				}
				if (args.data.message) {
					try {
							toastr.error(args.data.message.toString() + "Details: " + args.data.errMsg.toString(), "Error");
					} catch (e) {
						if(args.data.message.toString() == "Invalid accessToken.")
							return $location.path('/');
						toastr.error(args.data.message.toString(), "Error");
					}
				} else {
					toastr.error("Unknown error. Please try again", "Error");
				}
				return args.message;
			},

			get: function (endpoint) {
				var self = this;
				endpoint = system[config.ENVIRONMENT].API_URL + endpoint;
				if (endpoint && endpoint.indexOf('?') > -1 && localStorage.accessToken) {
					endpoint += '&accessToken=' + localStorage.accessToken;
				} else if (localStorage.accessToken) {
					endpoint += '?accessToken=' + localStorage.accessToken;
				}
				return $http.get(endpoint).then(self.ajaxSuccess, self.ajaxError);
			},

			post: function (endpoint, params) {
				var self = this;
				endpoint = system[config.ENVIRONMENT].API_URL + endpoint;
				if (localStorage.accessToken) params.accessToken = localStorage.accessToken;
				return $http.post(endpoint, params).then(self.ajaxSuccess, self.ajaxError);
			},

			put: function (endpoint, params) {
				var self = this;
				endpoint = system[config.ENVIRONMENT].API_URL + endpoint;
				if (localStorage.accessToken) params.accessToken = localStorage.accessToken;
				console.dir(params);
				return $http.put(endpoint, params).then(self.ajaxSuccess, self.ajaxError);
			},

			delete: function (endpoint, params) {
				var self = this;
				endpoint = system[config.ENVIRONMENT].API_URL + endpoint;

				if (localStorage && localStorage.accessToken)
					params.accessToken = localStorage.accessToken;

				if (endpoint && endpoint.indexOf('?') > -1 && localStorage.accessToken) {
					endpoint += '&accessToken=' + localStorage.accessToken;
				} else if (localStorage.accessToken) {
					endpoint += '?accessToken=' + localStorage.accessToken;
				}

				return $http.delete(endpoint, params).then(self.ajaxSuccess, self.ajaxError);
			}
		}

		return AjaxService;
	});
