var convertToBase64 = function (source, target) {
	var filesSelected = document.getElementById(source).files;

	if (filesSelected.length > 0) {
		var fileToLoad = filesSelected[0];

		var fileReader = new FileReader();

		fileReader.onload = function(fileLoadedEvent) {
			var srcData = fileLoadedEvent.target.result;
			document.getElementById(target).value = srcData;
			$('#' + target).val(srcData);
		}

		fileReader.readAsDataURL(fileToLoad);
	}
};

var imgToBase64 = function (source, target) {
    var filesSelected = document.getElementById(source).files;
	if (filesSelected.length > 0) {
		var fileToLoad = filesSelected[0];
		var fileReader = new FileReader();

		fileReader.onload = function(fileLoadedEvent) {
			var srcData  = fileLoadedEvent.target.result;
			var dataType = srcData.split(';')[0].split(':')[1];
			if(_.contains([ 'image/jpeg', 'image/jpg', 'image/png' ], dataType)){
				document.getElementById(target).value = srcData;
				$('#' + target).val(srcData);
			} else {
				alert("File type not supported!");
				document.getElementById(source).value = '';
				$('#' + source).val('');
				document.getElementById(target).value = '';
				$('#' + target).val('');
			}
		}

		fileReader.readAsDataURL(fileToLoad);
	}
}

