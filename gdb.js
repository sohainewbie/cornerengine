var cheerio = require('cheerio');
var bcrypt = require('bcrypt');
/**
* Debuging parse
* sohai
* references : https://www.digitalocean.com/community/tutorials/how-to-use-node-js-request-and-cheerio-to-set-up-simple-web-scraping
*/
/*
<li id="product-6287299" class="product _product  " data-productid="6287299" data-title="">
	<a class="item _item" href="https://www.zara.com/id/en/checked-dress-with-buttons-p09479084.html?v1=6287299&amp;v2=805003">
		<div class="product-grid-xmedia _product-grid-xmedia" style="padding-bottom: 124%;">
			<img id="product-img-6287299" class="product-media _img" alt="CHECKED DRESS WITH BUTTONS" src="https://static.zara.net/stdstatic/1.66.0-b.31/images/transparent-background.png">
			</div>
		</a>
		<div class="product-info _product-info">
			<div class="product-info-item product-info-item-label"></div>
			<div class="product-info-item product-info-item-name">
				<a class="name _item" href="https://www.zara.com/id/en/checked-dress-with-buttons-p09479084.html?v1=6287299&amp;v2=805003" aria-hidden="true" tabindex="-1">CHECKED DRESS WITH BUTTONS</a>
			</div>
			<div class="product-info-item product-info-item-price">
				<div class="price _product-price">
					<span data-price="999,900 IDR"></span>
				</div>
			</div>
			<div class="product-info-item product-info-item-hashtag"></div>
		</div>
	</li>
*/
let html = `<li id="product-6287299" class="product _product  " data-productid="6287299" data-title=""><a class="item _item" href="https://www.zara.com/id/en/checked-dress-with-buttons-p09479084.html?v1=6287299&amp;v2=805003"><div class="product-grid-xmedia _product-grid-xmedia" style="padding-bottom: 124%;"><img id="product-img-6287299" class="product-media _img" alt="CHECKED DRESS WITH BUTTONS" src="https://static.zara.net/stdstatic/1.66.0-b.31/images/transparent-background.png"></div></a><div class="product-info _product-info"><div class="product-info-item product-info-item-label"></div><div class="product-info-item product-info-item-name"><a class="name _item" href="https://www.zara.com/id/en/checked-dress-with-buttons-p09479084.html?v1=6287299&amp;v2=805003" aria-hidden="true" tabindex="-1">CHECKED DRESS WITH BUTTONS</a></div><div class="product-info-item product-info-item-price"><div class="price _product-price"><span data-price="999,900 IDR"></span></div></div><div class="product-info-item product-info-item-hashtag"></div></div></li>`;

   var $ = cheerio.load(html);
    $('div.product-info-item-hashtag').each(function(i, element){
      var price = $(this).prev().find('._product-price').find('span').attr('data-price');
      var link = $(this).prev().prev().find('._item').attr('href').split("?")[0];
      //var rank = a.parent().text();
      console.log(`${link}`)
    });


var salt = bcrypt.genSaltSync(10);
var hash = bcrypt.hashSync("s0h@iNewb13", 10);
console.log("hash=>" , hash);
