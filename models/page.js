/** --------------------------------------------
*
*   SAMPLE MODELS FOR MONGODB
*   corner
-------------------------------------------- **/

import Settings from 'settings';

const databaseConfig = new Settings(`${__dirname}/../config/config`);

let Mongoose  = require('mongoose').createConnection(databaseConfig.mongodb.corner.hostAddress),
    Schema    = require('mongoose').Schema;

let pageSchema = new Schema({
  title: { type: String, default: null },
  url: { type: String, required: true },
  urlImage: { type: String, default: null },
  color: { type: String, default: null },
  price: { 
    number: { type: Number, default: null },
    string: { type: String, default: null },
  },
  desc: { type: String, default: null },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  deletedAt: { type: Date, default: null }
});

module.exports = Mongoose.model('page', pageSchema);
