console.time('StartServer');
console.time('requireLib');
import express from 'express';
import Pipa from 'pipa';
//import PipaGateway from 'pipa-gateway';
import bodyParser from 'body-parser';
import expressLogger from 'express-request-logger';
import Winston from 'winston';
import cors from 'cors';
import Settings from 'settings';
import constant from './config/constant.js';
import compression from 'compression';
// import ejs from 'ejs';


let ejs = require('ais-ejs-mate')({ open: '{{', close: '}}' });

console.timeEnd('requireLib');

const config = new Settings(`${__dirname}/config/config`);
const app = express();


console.time("configLogger");
const log = new (Winston.Logger)({
  transports: [
    new (Winston.transports.Console)({ colorize: true, level: 'debug' }),
    new (Winston.transports.File)({ filename: `${__dirname}/logs/apps.log`, handleExceptions: true, colorize: true })
  ],
  exceptionHandlers: [
    new (Winston.transports.Console)({ colorize: true, level: 'warn' }),
    new (Winston.transports.File)({ filename: `${__dirname}/logs/exceptions.log`, handleExceptions: true, colorize: true })
  ]
});

app.use((req, res, next) => { 
  req.log = log;
  next();
 })

console.timeEnd("configLogger");


const corsOpts = {
  origin(origin, callback) {
    const originIsWhitelisted = constant.originWhitelist.includes(origin);
    callback(originIsWhitelisted ? null : 'Bad Request', originIsWhitelisted)
  }
};
app.use(cors());

console.time("setupCompression");

function shouldCompress (req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }
 
  // fallback to standard filter function
  return compression.filter(req, res)
}

app.use(compression({filter: shouldCompress})) //trying to compress json response

console.time("configViewEngine")
/*app.engine('ejs', ejs);
app.set('views', `${__dirname}/application/views`);
app.set('view engine', 'ejs');
app.use('/assets', require('express').static(`${__dirname}/application/views/assets`));
console.timeEnd("configViewEngine")
*/

app.engine('ejs', ejs)
app.set('views', `${__dirname}/views`)
app.set('view engine', 'ejs')
//app.use('/admin/assets', require('express').static(`${__dirname}/views/engine/assets`))
app.use('/engine/assets', require('express').static(`${__dirname}/views/engine/assets`))
app.use('/engine/application', require('express').static(`${__dirname}/views/engine/application`))

console.timeEnd("configViewEngine")


console.time("configRouter")
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ extended: true, limit: '50mb' }));
app.use(expressLogger.create(log))
app.disable('x-powered-by')

let pipa = new Pipa(app, 'routes', 'middlewares')
//let pipaGateway = new PipaGateway(app, {configPath:'./routes-config.json',middlewarePath:'./middlewares'})
// Open pipa
pipa.open();
// Open the Pipa Gateway
//pipaGateway.open();
console.timeEnd("configRouter");


app.listen(config.port);

console.timeEnd('StartServer');
log.debug('---------------------------------------');
log.debug(' CORNER API GATEWAY STARTED AT PORT: ' + config.port);
log.debug('---------------------------------------');
