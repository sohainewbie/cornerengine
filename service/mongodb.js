/** --------------------------------------------
*
*   MONGO SERVICES
*
-------------------------------------------- **/

'use strict';

let _ = require('lodash');

class Mongo {

  constructor (modelName) {
    this.model = require(`${__dirname}/../models/${modelName}`)
  }

  /** --------------------------------------------
  *
  *   find - Find with opts
  *
  -------------------------------------------- **/

  find (opts, cb) {
    opts = _.assignIn(opts, { deletedAt: null });
    this.model.find(opts, cb);
  }

  /** --------------------------------------------
  *
  *   findOne - Find one-record with opts
  *
  -------------------------------------------- **/

  findOne (opts, cb) {
    opts = _.assignIn(opts, { deletedAt: null });
    this.model.findOne(opts, cb);
  }

  /** --------------------------------------------
  *
  *   findById - Find by _id
  *
  -------------------------------------------- **/  

  findById (id, cb) {
    let opts = { _id: id, deletedAt: null };
    this.model.findOne(opts, cb);
  }

  /** --------------------------------------------
  *
  *   create - Create a new record
  *
  -------------------------------------------- **/  

  create (opts, cb) {
    let model = new this.model(opts);
    model.save(cb);
  }

  /** --------------------------------------------
  *
  *   remove - Remove a new record by query
  *
  -------------------------------------------- **/  

  remove (opts, cb) {
    opts = _.assignIn(opts, { deletedAt: null });
    let setOpts = { deletedAt: new Date() };
    let attrOpts = { multi: true };
    this.model.update(opts, setOpts, attrOpts, cb);
  }

  /** --------------------------------------------
  *
  *   remove - Remove a record by _id
  *
  -------------------------------------------- **/  

  removeById (id, cb) {
    let opts = { _id: id, deletedAt: null };
    let setOpts = { deletedAt: new Date() };
    let attrOpts = { multi: false };
    this.model.update(opts, setOpts, attrOpts, cb);
  }

  /** --------------------------------------------
  *
  *   update - Update on or more record
  *
  -------------------------------------------- **/  

  update (opts, setOpts, attrOpts, cb) {
    opts = _.assignIn(opts, { deletedAt: null });
    setOpts = _.assignIn(setOpts, { updatedAt: new Date() });
    this.model.update(opts, setOpts, attrOpts, cb);
  }

  /** --------------------------------------------
  *
  *   count - Count by query
  *
  -------------------------------------------- **/  

  count (opts, cb) {
    opts = _.assignIn(opts, { deletedAt: null });
    this.model.count(opts, cb);
  }

}

module.exports = Mongo;