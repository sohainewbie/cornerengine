#Crawling Engine v 1.0.0
```
	This tools purpose for Crawling data and save to database mongodb.
	Frontend with anggular 1.0

	for development  i create 3 environment:
		1. common      : this env for debuging/development at your local
		2. development : this env for testing before lauch to production
		3. production  : this env for production
		
```

# HOW TO SETUP THIS APPS

```
$ npm install / yarn install

NODE_ENV=[environment] node index.js
ex: 
	$ NODE_ENV=common node index.js
```

#HOW TO SETUP FRONTEND 
```
$ cd /views/engine/assets
$ bower install
  -> 10) angular#1.4.8 which resolved to 1.4.8 and is required by cornerengine-angular-skeleton
```

# DOCUMENTATION API

#FETCHING URL
```
URL=>http://localhost:8080/v1/crawler/fetchdata?url=https://www.zara.com/id/en/woman-new-in-l1180.html?v1=805003
PARAMS-OPTIONS= &bot=true
METHOD=>GET
RESPONSE=>
	{
    	"message": "Success"
	}
```

#VIEW DATA
```
URL=>http://localhost:8080/v1/page/dataview
METHOD=>GET
RESPONSE=>
		{
		    "message": "Success",
		    "data": [
		        {
		            "_id": "5ae498560288ae00946edce2",
		            "url": "https://www.zara.com/id/en/midi-dress-with-front-knot-p04437073.html?v1=6092076&v2=805003",
		            "deletedAt": null,
		            "updatedAt": "2018-04-28T15:50:46.929Z",
		            "createdAt": "2018-04-28T15:50:46.929Z",
		            "desc": null,
		            "price": {
		                "string": "999,900 IDR",
		                "number": 999900
		            },
		            "color": null,
		            "urlImage": null,
		            "title": null,
		            "__v": 0
		        },{...}
		    ]
		}
```


#HOMEPAGE ADMIN
```
http://localhost:8080/engine/#/login
USER:superadmin
PASS:Eng1n3$$
```
